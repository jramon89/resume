function Events() {
    this.observers = [];
}
Events.prototype.suscribe = function(call) {
    this.observers.push(call);
}
Events.prototype.unsuscribe = function(call) {
    if (this.observers.length) {
        var index = this.observers.indexOf(call);
        this.observers.splice(index, 1);
    }
}
Events.prototype.notifyObserver = function(value) {

    if (this.observers.length) {
        this.observers[0](value);
    }
}

function Lang() {
	this.events = new Events();
    this.config = new Config();
	this.isES = this.config.get('config') && this.config.get('config').lang === 'en' ? false : true,
    this.lang = 'es';
}


Lang.prototype.switchLang = function() {
	this.isES = !this.isES;
    this.lang = this.isES ? 'es' : 'en';
	this.events.notifyObserver(this.lang);

    return this.lang;
}


Lang.prototype.getLang = function() {
    return this.isES;
}

//var ev = new Events();


/*
function notify(fn) {
	ev.suscribe(fn);
	ev.notifyObserver();
}
*/

/*
var events = {
	isES: true,
    setLang: function() {
    	this.isES = !this.isES;
    },
    getLang: function() {
    	return this.isES
    }
}

*/