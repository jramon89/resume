function handler() {
    this.view = null;

    this.setView = function(view) {
        var self = this;

        var req = new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', view);
            xhr.send();
            xhr.responseType = 'text';
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(xhr);
                    } else {
                        reject('Error retrieve html')
                    }
                }
            }

        });

        this.view = req.then(function(data) {
            return data.response;
        });
    }

    this.getView = function() {
        return this.view;
    }
}

function Config() {
    this.data = null;
    this.val = null;
    this.storageValue = null;
    /*
    this.setData = function(file) {
        var request = new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', file);
            xhr.responseType = 'json',
                xhr.onload = function() {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            this.val = xhr;
                            console.log('xhr', xhr)
                            resolve(xhr);
                        } else {
                            reject('Error retrieve data');
                        }
                    }
                }
            xhr.send(null);
        });

        this.data = request.then(function(data) {
            return data.response;
        });
    }

    this.getData = function() {
        return this.data;
    }
    */

    this.requestData = function(file) {
        var self = this;
        var xhr = new XMLHttpRequest();
        xhr.overrideMimeType('text/html');
        xhr.open('GET', file, false);
        xhr.setRequestHeader('Content-Type', 'text/plain');
        // xhr.responseType = 'json';
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    var data = xhr.response;
                    self.setData(JSON.parse(data));
                } else {
                    console.log('Error retrieve data');
                }
            }
        }
        xhr.send(null);
    }

    this.setData = function(data) {
        this.data = data
    }

    this.getData = function() {
        return this.data;
    }

    /*
        localStorage configuation
    */

    this.set = function(name, value) {
        this.storageValue = value;
        var val = this.storageValue;
        if (typeof this.storageValue === 'object') {
            val = JSON.stringify(value);
        }

        localStorage.setItem(name, val);
    }

    this.get = function(name) {

        return typeof this.storageValue === 'object' ?
            JSON.parse(localStorage.getItem(name)) :
            localStorage.getItem(name);
    }

    this.remove = function(name) {
        localStorage.removeItem(name)
    }

    /*
        Browser detection
    */

    this.getBrowser = function() {
        var navAgenet = window.navigator.userAgent,
            browsers = ['Chrome', 'Firefox', 'Opera', 'Safari', 'Edge', 'MSIE'],
            browser = 'Edge';

        for (var i = browsers.length - 1; i >= 0; i--) {
            if (navAgenet.indexOf(browsers[i]) > -1 && navAgenet.indexOf('Edge') === -1) {
                browser = browsers[i]
            }
        }

        return browser;
    }

    this.getLangLocation = function() {
        var navAgent = window.navigator.languages,
            lang = navAgent[0].split('-');

        return lang[0];
    }

    this.time = function(element, call) {
        var dateTime = new Date(),
            date = dateTime.getFullYear(),
            hours = dateTime.getHours(),
            minutes = dateTime.getMinutes(),
            seconds = dateTime.getSeconds(),
            filterTime = function(time) {
                var t = time;
                if (time <= 9) {
                    t = `0${time}`;
                }
                return t;
            }
        /*
        setInterval(function() {
           
            seconds++;

            if (seconds === 59) {
                minutes = minutes + 1;
                seconds = 0;
                if (minutes === 60) {
                    hours = hours + 1;
                    minutes = 0;
                }
                if (hours > 24) {
                    hours = 1;
                }
            }
            return `${filterTime(hours)}:${filterTime(minutes)}:${filterTime(seconds)}`;
            
            //document.getElementById(element)
            //    .innerHTML = `${filterTime(hours)}:${filterTime(minutes)}:${filterTime(seconds)}`;
        }, 1000);
        */
        return (function() {
            seconds++;

            if (seconds === 59) {
                minutes = minutes + 1;
                seconds = 0;
                if (minutes === 60) {
                    hours = hours + 1;
                    minutes = 0;
                }
                if (hours > 24) {
                    hours = 1;
                }
            }
            return `${filterTime(hours)}:${filterTime(minutes)}:${filterTime(seconds)}`;
        })();
    }
}