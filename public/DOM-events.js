var DOMevents = {
    viewValue: null,
    browser: null,
    config: new Config(),
    lang: new Lang(),

    initDOMvalues: function() {
        if (localStorage.getItem('color')) {
            this.setTemplateColor(undefined);
            // document.body.style.setProperty('--color', JSON.parse(localStorage.getItem('color')).color);
        }
        // this.config.time('time');
        setInterval(function() {
            document.getElementById('time')
                .innerHTML = this.config.time('time');
        }.bind(this), 1000);

        this.browser = this.config.getBrowser();
        this.wResize();
    },
    carrouselControlls: function(value) {
        var element = document.querySelector('.images');
        var aElement = null;

        for (var i = element.children.length - 1; i >= 0; i--) {

            if (element.children[i].classList.contains('active')) {
                aElement = element.children[i][value === 'next' ? 'nextElementSibling' : 'previousElementSibling'];
            }

            element.children[i].classList.remove('active');
        }

        if (!aElement) {
            element.children[value === 'next' ? 0 : element.children.length - 1].classList.add('active');
        } else {
            aElement.classList.add('active');
        }
    },

    setTemplateColor: function(e) {
        var colorValue = e ? e.value : null || this.config.get('color').color;

        var setValue = function(arr, rule) {
                for (var i = arr.length - 1; i >= 0; i--) {
                    arr[i].style[rule] = colorValue;
                }
            },
            background = document.getElementsByClassName('colorLayout'),
            color = document.getElementsByClassName('textColorLayout');



        this.config.set('color', { color: colorValue });

        if (e && e.value) {
            render(this.config.get('config').lang);
        }

        // setValue(background, 'backgroundColor');
        // setValue(color, 'color');

        setTimeout(function() {
            if (document.getElementById('tColor')) {
                document.getElementById('tColor').value = colorValue;
            }
            document.body.style.setProperty('--color', colorValue);
        }, 10);


    },
    getValueFromView: function(value) {
        this.viewValue = value;
    },
    printResume: function(lang) {
        var msg = lang === 'es' ? 'Para mejor calidad de impresión se recomienda usar Google chrome' :
            'For better quality printing, please use Google chrome';

        if (this.browser === 'Edge' || this.browser === 'Firefox') {
            if (confirm(msg)) {
                alert('Sorry we cannot open page in chrome :( \n please open app manually in Chrome browser');
                window.open('https://www.google.com/chrome');
            } else {
                window.print();
            }
        } else {
            window.print();
        }
    },
    resetTemplateColor: function() {
        this.setTemplateColor({ value: '#5f9ea0' });
        this.config.remove('color')
        render(this.config.get('config').lang);
    },
    next: function() {
        this.carrouselControlls('next');
    },
    prev: function() {

        this.carrouselControlls('prev');
    },

    widgetEvent: function() {
        document.getElementById('container')
            .classList.toggle('mobile');
    },

    wResize: function() {
        var w = document.getElementById('container');
        window.addEventListener('resize', function() {
            if (window.innerWidth > 760) {
                if (w.classList.contains('mobile')) {
                    w.classList.remove('mobile')
                }
            }
        }.bind(this));
    }

}