function handler() {
    this.view = null;

    this.setView = function(view) {
        var self = this;

        var req = new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', view);
            xhr.send();
            xhr.responseType = 'text';
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(xhr);
                    } else {
                        reject('Error retrieve html')
                    }
                }
            }

        });

        this.view = req.then(function(data) {
            return data.response;
        });
    }

    this.getView = function() {
        return this.view;
    }
}

function Config() {
    this.data = null;
    this.val = null;
    this.storageValue = null;
    /*
    this.setData = function(file) {
        var request = new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', file);
            xhr.responseType = 'json',
                xhr.onload = function() {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            this.val = xhr;
                            console.log('xhr', xhr)
                            resolve(xhr);
                        } else {
                            reject('Error retrieve data');
                        }
                    }
                }
            xhr.send(null);
        });

        this.data = request.then(function(data) {
            return data.response;
        });
    }

    this.getData = function() {
        return this.data;
    }
    */

    this.requestData = function(file) {
        var self = this;
        var xhr = new XMLHttpRequest();
        xhr.overrideMimeType('text/html');
        xhr.open('GET', file, false);
        xhr.setRequestHeader('Content-Type', 'text/plain');
        // xhr.responseType = 'json';
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    var data = xhr.response;
                    self.setData(JSON.parse(data));
                } else {
                    console.log('Error retrieve data');
                }
            }
        }
        xhr.send(null);
    }

    this.setData = function(data) {
        this.data = data
    }

    this.getData = function() {
        return this.data;
    }

    /*
        localStorage configuation
    */

    this.set = function(name, value) {
        this.storageValue = value;
        var val = this.storageValue;
        if (typeof this.storageValue === 'object') {
            val = JSON.stringify(value);
        }

        localStorage.setItem(name, val);
    }

    this.get = function(name) {

        return typeof this.storageValue === 'object' ?
            JSON.parse(localStorage.getItem(name)) :
            localStorage.getItem(name);
    }

    this.remove = function(name) {
        localStorage.removeItem(name)
    }

    /*
        Browser detection
    */

    this.getBrowser = function() {
        var navAgenet = window.navigator.userAgent,
            browsers = ['Chrome', 'Firefox', 'Opera', 'Safari', 'Edge', 'MSIE'],
            browser = 'Edge';

        for (var i = browsers.length - 1; i >= 0; i--) {
            if (navAgenet.indexOf(browsers[i]) > -1 && navAgenet.indexOf('Edge') === -1) {
                browser = browsers[i]
            }
        }

        return browser;
    }

    this.getLangLocation = function() {
        var navAgent = window.navigator.languages,
            lang = navAgent[0].split('-');

        return lang[0];
    }

    this.time = function(element, call) {
        var dateTime = new Date(),
            date = dateTime.getFullYear(),
            hours = dateTime.getHours(),
            minutes = dateTime.getMinutes(),
            seconds = dateTime.getSeconds(),
            filterTime = function(time) {
                var t = time;
                if (time <= 9) {
                    t = `0${time}`;
                }
                return t;
            }
        /*
        setInterval(function() {
           
            seconds++;

            if (seconds === 59) {
                minutes = minutes + 1;
                seconds = 0;
                if (minutes === 60) {
                    hours = hours + 1;
                    minutes = 0;
                }
                if (hours > 24) {
                    hours = 1;
                }
            }
            return `${filterTime(hours)}:${filterTime(minutes)}:${filterTime(seconds)}`;
            
            //document.getElementById(element)
            //    .innerHTML = `${filterTime(hours)}:${filterTime(minutes)}:${filterTime(seconds)}`;
        }, 1000);
        */
        return (function() {
            seconds++;

            if (seconds === 59) {
                minutes = minutes + 1;
                seconds = 0;
                if (minutes === 60) {
                    hours = hours + 1;
                    minutes = 0;
                }
                if (hours > 24) {
                    hours = 1;
                }
            }
            return `${filterTime(hours)}:${filterTime(minutes)}:${filterTime(seconds)}`;
        })();
    }
}
function Article(data) {
    this.data = data;
    this.html = `<ul>
                    <li>
                        <div class="main-title textColorLayout">
                            ${ this.data.experience.title }
                        </div>
                        <ul class="list">
                             ${ 
                                this.data.experience.content.map(function(val){
                                    return `<li>${val.position}, ${val.company}, ${val.city.state}, ${val.city.Country}, ${val.startDate} - ${val.endDate} </li>`;
                                }).join('')
                             }
                        </ul>
                    </li>
                </ul>
                <ul>
                    <li>
                        <div class="main-title textColorLayout">
                            ${ this.data.knowledge.title }
                        </div>
                        <ul>
                            <li>
                                <div class="sub-title textColorLayout">
                                    ${ this.data.knowledge.content.dev.title }
                                </div>
                                <ul>
                                    ${ 
                                        this.data.knowledge.content.dev.content.map(function(val){
                                            return `<li>
                                                        <div class="title">
                                                           ${ val.title }
                                                        </div>
                                                        <ul class="list">
                                                           ${
                                                                val.content.map(function(val){
                                                                     return `<li>${val}</li>`;  
                                                                }).join('')
                                                            }
                                                        </ul>
                                                    </li>`;
                                        }).join('') 
                                    }
                                </ul>
                            </li>
                            <li>
                                <div class="sub-title textColorLayout">
                                    ${ this.data.knowledge.content.net.title }

                                </div>
                                <ul class="list">
                                    ${
                                        this.data.knowledge.content.net.content.map(function(val){
                                            return `<li>${val}</li>`;
                                        }).join('')
                                    }
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul>
                    <li>
                        <div class="main-title textColorLayout">
                            ${ this.data.languajes.title }
                        </div>
                        <ul class="list">
                            ${
                                this.data.languajes.content.map(function(val){
                                    return `<li>${val}</li>`;
                                }).join('')
                             }
                        </ul>
                    </li>
                </ul>`;

    this.renderView = function() {
        return this.html;
    }
}
function Banner(data) {
    this.data = data;
    this.html = `<div class="icons-wrap">
                    <div class="icons-left" onclick="DOMevents.widgetEvent()"><i class="bar"></i></div>
                    <div class="icons-right">
                        <div class="rstColor hm">
                      ${ DOMevents.config.get('config') ? 
                      `<span> You're using ${DOMevents.config.get('config').browser} browser</span>`: '' }
                        / time:
                        </div>&nbsp;  
                        <div class="rstColor hm" id="time">
                            ${ 
                               !DOMevents.time ? `<small>Loading..</small>` : DOMevents.time
                            }
                        </div>&nbsp <span class="hm">|</span>
                        <div class="icon icon-print">
                            <i class="fas fa-print" onclick=${this.data.config.language === "es" ? "DOMevents.printResume('es')"
                            : "DOMevents.printResume('en')"}
                            title="${ this.data.DOMinfo.bannerIcons.print.title }"></i>
                        </div>
                        <div class="icon icon-lang">
                            <i class="fas ${this.data.config.language === "es" ? "fa-toggle-off" : "fa-toggle-on"}" onclick="Lang.switchLang()" title="${ this.data.DOMinfo.bannerIcons.lang.title }"></i>
                        </div>
                        <div class="lang">
                            <span>${ this.data.config.language.toUpperCase() }</span>
                        </div>
                        ${ DOMevents.config.get('config') && 
                            DOMevents.config.get('config').browser !== 'Safari' ?  
                            `<div class="icon">
                                <input type="color"id="tColor"  name="color" value="#5f9ea0" onchange="DOMevents.setTemplateColor(this)" title="${ this.data.DOMinfo.bannerIcons.color.title }">
                            </div>` : ''
                        }
                        <div class="rstColor" onclick="DOMevents.resetTemplateColor()">
                        ${
                            localStorage.getItem('color') && DOMevents.config.get('config').browser !== 'Safari'? 
                                this.data.config.language === "en" ? 
                                    "<span>Reset color</span>":
                                    "<span>Restablecer color</span>" : ''
                        }
                        </div>
                        </div>
                </div>`;

    this.renderView = function() {
        return this.html;
    }
}
function Layout(views) {
    this.html = `<section class="container colorLayout" id="container">
    <aside class="widget colorLayout" id="widget">
        <div class="widgetBanner colorLayout">
            <div class="imageProfile colorLayout">
                <span onclick="DOMevents.prev()" class="controls"><i class="fas fa-chevron-left"></i></span>
                <div class="images">
                    ${ views['image'].render() }
                </div>

                <span onclick="DOMevents.next()" class="controls"><i class="fas fa-chevron-right"></i></span>
            </div>
        </div>
        <div class="widgetContent">
            <div class="widgetInfo">

                ${ views['profile'].render() }
                ${ views['widget'].render() }
                
            </div>
        </div>
    </aside>
    <aricle class="article">
        <div class="articleContent">
            <div class="banner colorLayout">

                ${ views['banner'].render() }
                
            </div>
            <div class="experience">
                ${ views['article'].render() }
            </div>
        </div>
    </aricle>
</section>`;

    this.renderView = function() {
        return this.html;
    }
}
function Profile(data) {
    this.data = data;
    this.html = `<div class="name">
                    <div class="title">
                        ${ this.data.name }
                    </div>
                    <small>
                        ${ this.data.career }
                        </small>
                </div>
                <ul class="line">
                    <li>
                        <hr style="width: 100%" />
                    </li>
                </ul>`;

    this.renderView = function() {
        return this.html;
    }
}
function ProfileImage(data) {
    this.data = data;
    this.html = `
                    ${
                        this.data.map(function(val){
                            return `
                                    <div role="img" class="image ${val.active ? "active" : ''}" 
                                        style="background: url(${val.img}) center no-repeat; background-size:cover" aria-labe="Jose Ramon R">

                                    </div>`;
                        }).join('')
                    }
                    `;

    this.renderView = function() {
        return this.html;
    }
}
function Widget(data) {
    //console.log('DATA', data);
	this.data = data;
    this.html = `<ul class="list-info">
                    <li>
                        <div class="title">
                            ${ this.data.about.title }
                        </div>
                        <ul>
                            <li>
                                ${ this.data.about.content }
                            </li>
                        </ul>
                        <div class="title">
                            ${ this.data.contact.title }
                        </div>
                        <ul>
                            <li>
                                ${ this.data.contact.content.birthDate.title }: ${ this.data.contact.content.birthDate.content }, ${ this.data.contact.content.birthPlace.state }, 
                                ${ this.data.contact.content.birthPlace.Country }
                            </li>
                            <li>
                                ${ this.data.contact.content.currentAddress.title }: ${ this.data.contact.content.currentAddress.content }
                            </li>
                            <li>
                                ${ this.data.contact.content.phones.title }: ${ this.data.contact.content.phones.content[0] } / 
                                ${ this.data.contact.content.phones.content[1] }
                            </li>
                            <li>
                                ${ this.data.contact.content.email.title }: ${ this.data.contact.content.email.content }
                            </li>
                        </ul>
                        <ul class="social">
                            ${ 
                                this.data.contact.content.social.map(function(val, i){
                                    return `<li class="c-${i}"><i class="fab ${val.class}"></i>
                                                <span><a href="${val.link}" target="_BLANK" title="${val.user}">${val.user}</a></span>
                                            </li>`;
                                }).join('')
                            }
                        </ul>
                    </li>
                    <li>
                        <div class="title">
                           ${ this.data.school.title }
                        </div>
                        ${ 
                            this.data.school.content.map(function(val,key){
                                return `<ul><li>${val.title} / ${val.city.state}, ${val.city.Country}</li></ul>`
                            }).join('') 
                        }
                    </li>
                </ul>`;

    this.renderView = function() {
        return this.html;
    }
}
function ArticleController(path) {
    this.path = path;
    this.view = null;
}

ArticleController.prototype = new Config();
ArticleController.prototype.render = function() {

	this.requestData(this.path);
	return new Article(this.getData().article)
            .renderView();
	/*
    this.setData(this.path);
    this.view = this.getData().then(function(data) {
       return new Widget(data)
            .renderView();
    });
    */
}
function BannerController(path) {
    this.path = path;
}

BannerController.prototype = new Config();
BannerController.prototype.render = function() {
    this.requestData(this.path);
    return new Banner(this.getData())
        .renderView();
}
function LayoutController(views) {
    // this.path = path;
    this.views = views;
}

// LayoutController.prototype = new Config();
LayoutController.prototype.render = function() {
    // this.requestData(this.path);
    return new Layout(this.views)
        .renderView();
}
function ProfileController(path) {
    this.path = path;
}

ProfileController.prototype = new Config();
ProfileController.prototype.render = function() {
    this.requestData(this.path);
    return new Profile(this.getData().widget.profile)
        .renderView();
}
function ProfileImageController(path) {
    this.path = path;
}

ProfileImageController.prototype = new Config();
ProfileImageController.prototype.render = function() {
    this.requestData(this.path);
    return new ProfileImage(this.getData().widget.profile.images)
        .renderView();
}
function WidgetController(path) {
    this.path = path;
    this.view = null;
}

WidgetController.prototype = new Config();
WidgetController.prototype.render = function() {

	this.requestData(this.path);
	return new Widget(this.getData().widget)
            .renderView();
	/*
    this.setData(this.path);
    this.view = this.getData().then(function(data) {
       return new Widget(data)
            .renderView();
    });
    */
}
function handler() {
    this.view = null;

    this.setView = function(view) {
        var self = this;

        var req = new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', view);
            xhr.send();
            xhr.responseType = 'text';
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(xhr);
                    } else {
                        reject('Error retrieve html')
                    }
                }
            }

        });

        this.view = req.then(function(data) {
            return data.response;
        });
    }

    this.getView = function() {
        return this.view;
    }
}

function Config() {
    this.data = null;
    this.val = null;
    this.storageValue = null;
    /*
    this.setData = function(file) {
        var request = new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', file);
            xhr.responseType = 'json',
                xhr.onload = function() {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            this.val = xhr;
                            console.log('xhr', xhr)
                            resolve(xhr);
                        } else {
                            reject('Error retrieve data');
                        }
                    }
                }
            xhr.send(null);
        });

        this.data = request.then(function(data) {
            return data.response;
        });
    }

    this.getData = function() {
        return this.data;
    }
    */

    this.requestData = function(file) {
        var self = this;
        var xhr = new XMLHttpRequest();
        xhr.overrideMimeType('text/html');
        xhr.open('GET', file, false);
        xhr.setRequestHeader('Content-Type', 'text/plain');
        // xhr.responseType = 'json';
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    var data = xhr.response;
                    self.setData(JSON.parse(data));
                } else {
                    console.log('Error retrieve data');
                }
            }
        }
        xhr.send(null);
    }

    this.setData = function(data) {
        this.data = data
    }

    this.getData = function() {
        return this.data;
    }

    /*
        localStorage configuation
    */

    this.set = function(name, value) {
        this.storageValue = value;
        var val = this.storageValue;
        if (typeof this.storageValue === 'object') {
            val = JSON.stringify(value);
        }

        localStorage.setItem(name, val);
    }

    this.get = function(name) {

        return typeof this.storageValue === 'object' ?
            JSON.parse(localStorage.getItem(name)) :
            localStorage.getItem(name);
    }

    this.remove = function(name) {
        localStorage.removeItem(name)
    }

    /*
        Browser detection
    */

    this.getBrowser = function() {
        var navAgenet = window.navigator.userAgent,
            browsers = ['Chrome', 'Firefox', 'Opera', 'Safari', 'Edge', 'MSIE'],
            browser = 'Edge';

        for (var i = browsers.length - 1; i >= 0; i--) {
            if (navAgenet.indexOf(browsers[i]) > -1 && navAgenet.indexOf('Edge') === -1) {
                browser = browsers[i]
            }
        }

        return browser;
    }

    this.getLangLocation = function() {
        var navAgent = window.navigator.languages,
            lang = navAgent[0].split('-');

        return lang[0];
    }

    this.time = function(element, call) {
        var dateTime = new Date(),
            date = dateTime.getFullYear(),
            hours = dateTime.getHours(),
            minutes = dateTime.getMinutes(),
            seconds = dateTime.getSeconds(),
            filterTime = function(time) {
                var t = time;
                if (time <= 9) {
                    t = `0${time}`;
                }
                return t;
            }
        /*
        setInterval(function() {
           
            seconds++;

            if (seconds === 59) {
                minutes = minutes + 1;
                seconds = 0;
                if (minutes === 60) {
                    hours = hours + 1;
                    minutes = 0;
                }
                if (hours > 24) {
                    hours = 1;
                }
            }
            return `${filterTime(hours)}:${filterTime(minutes)}:${filterTime(seconds)}`;
            
            //document.getElementById(element)
            //    .innerHTML = `${filterTime(hours)}:${filterTime(minutes)}:${filterTime(seconds)}`;
        }, 1000);
        */
        return (function() {
            seconds++;

            if (seconds === 59) {
                minutes = minutes + 1;
                seconds = 0;
                if (minutes === 60) {
                    hours = hours + 1;
                    minutes = 0;
                }
                if (hours > 24) {
                    hours = 1;
                }
            }
            return `${filterTime(hours)}:${filterTime(minutes)}:${filterTime(seconds)}`;
        })();
    }
}
var DOMevents = {
    viewValue: null,
    browser: null,
    config: new Config(),
    lang: new Lang(),

    initDOMvalues: function() {
        if (localStorage.getItem('color')) {
            this.setTemplateColor(undefined);
            // document.body.style.setProperty('--color', JSON.parse(localStorage.getItem('color')).color);
        }
        // this.config.time('time');
        setInterval(function() {
            document.getElementById('time')
                .innerHTML = this.config.time('time');
        }.bind(this), 1000);

        this.browser = this.config.getBrowser();
        this.wResize();
    },
    carrouselControlls: function(value) {
        var element = document.querySelector('.images');
        var aElement = null;

        for (var i = element.children.length - 1; i >= 0; i--) {

            if (element.children[i].classList.contains('active')) {
                aElement = element.children[i][value === 'next' ? 'nextElementSibling' : 'previousElementSibling'];
            }

            element.children[i].classList.remove('active');
        }

        if (!aElement) {
            element.children[value === 'next' ? 0 : element.children.length - 1].classList.add('active');
        } else {
            aElement.classList.add('active');
        }
    },

    setTemplateColor: function(e) {
        var colorValue = e ? e.value : null || this.config.get('color').color;

        var setValue = function(arr, rule) {
                for (var i = arr.length - 1; i >= 0; i--) {
                    arr[i].style[rule] = colorValue;
                }
            },
            background = document.getElementsByClassName('colorLayout'),
            color = document.getElementsByClassName('textColorLayout');



        this.config.set('color', { color: colorValue });

        if (e && e.value) {
            render(this.config.get('config').lang);
        }

        // setValue(background, 'backgroundColor');
        // setValue(color, 'color');

        setTimeout(function() {
            if (document.getElementById('tColor')) {
                document.getElementById('tColor').value = colorValue;
            }
            document.body.style.setProperty('--color', colorValue);
        }, 10);


    },
    getValueFromView: function(value) {
        this.viewValue = value;
    },
    printResume: function(lang) {
        var msg = lang === 'es' ? 'Para mejor calidad de impresión se recomienda usar Google chrome' :
            'For better quality printing, please use Google chrome';

        if (this.browser === 'Edge' || this.browser === 'Firefox') {
            if (confirm(msg)) {
                alert('Sorry we cannot open page in chrome :( \n please open app manually in Chrome browser');
                window.open('https://www.google.com/chrome');
            } else {
                window.print();
            }
        } else {
            window.print();
        }
    },
    resetTemplateColor: function() {
        this.setTemplateColor({ value: '#5f9ea0' });
        this.config.remove('color')
        render(this.config.get('config').lang);
    },
    next: function() {
        this.carrouselControlls('next');
    },
    prev: function() {

        this.carrouselControlls('prev');
    },

    widgetEvent: function() {
        document.getElementById('container')
            .classList.toggle('mobile');
    },

    wResize: function() {
        var w = document.getElementById('container');
        window.addEventListener('resize', function() {
            if (window.innerWidth > 760) {
                if (w.classList.contains('mobile')) {
                    w.classList.remove('mobile')
                }
            }
        }.bind(this));
    }

}
function Events() {
    this.observers = [];
}
Events.prototype.suscribe = function(call) {
    this.observers.push(call);
}
Events.prototype.unsuscribe = function(call) {
    if (this.observers.length) {
        var index = this.observers.indexOf(call);
        this.observers.splice(index, 1);
    }
}
Events.prototype.notifyObserver = function(value) {

    if (this.observers.length) {
        this.observers[0](value);
    }
}

function Lang() {
	this.events = new Events();
    this.config = new Config();
	this.isES = this.config.get('config') && this.config.get('config').lang === 'en' ? false : true,
    this.lang = 'es';
}


Lang.prototype.switchLang = function() {
	this.isES = !this.isES;
    this.lang = this.isES ? 'es' : 'en';
	this.events.notifyObserver(this.lang);

    return this.lang;
}


Lang.prototype.getLang = function() {
    return this.isES;
}

//var ev = new Events();


/*
function notify(fn) {
	ev.suscribe(fn);
	ev.notifyObserver();
}
*/

/*
var events = {
	isES: true,
    setLang: function() {
    	this.isES = !this.isES;
    },
    getLang: function() {
    	return this.isES
    }
}

*/
function App(lang) {
    this.lang = lang;
    this.path = {
        es: './data/es.json',
        en: './data/en.json',
    }
    // this.path = value === 'es' ? './data/es.json' : './data/en.json';
}

App.prototype = new Config();

App.prototype.views = function() {
    var path = this.lang ? this.path[this.lang] : this.path['es'];

    return {
        article: new ArticleController(path),
        widget: new WidgetController(path),
        profile: new ProfileController(path),
        banner: new BannerController(path),
        image: new ProfileImageController(path),
    }
}

App.prototype.render = function(view) {
    this.set('config', {
        lang: this.lang,
        browser: this.getBrowser()
    });

    return new LayoutController(this.views())
        .render();
}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbmZpZy5qcyIsImFydGljbGUtdmlldy5qcyIsImJhbm5lci12aWV3LmpzIiwibGF5b3V0LmpzIiwicHJvZmlsZS12aWV3LmpzIiwicHJvZmlsZUltYWdlLXZpZXcuanMiLCJ3aWRnZXQtdmlldy5qcyIsImFydGljbGUtY29udHJvbGxlci5qcyIsImJhbm5lci1jb250cm9sbGVyLmpzIiwibGF5b3V0LWNvbnRyb2xsZXIuanMiLCJwcm9maWxlLWNvbnRyb2xsZXIuanMiLCJwcm9maWxlSW1hZ2UtY29udHJvbGxlci5qcyIsIndpZGdldC1jb250cm9sbGVyLmpzIiwiRE9NLWV2ZW50cy5qcyIsImV2ZW50cy5qcyIsImFwcC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNyTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQy9FQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDOUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDdkNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNuQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDakJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDekRBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDbEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDVEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDVEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNUQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBWmxCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QWFyTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3RIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDN0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIGhhbmRsZXIoKSB7XHJcbiAgICB0aGlzLnZpZXcgPSBudWxsO1xyXG5cclxuICAgIHRoaXMuc2V0VmlldyA9IGZ1bmN0aW9uKHZpZXcpIHtcclxuICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgICAgIHZhciByZXEgPSBuZXcgUHJvbWlzZShmdW5jdGlvbihyZXNvbHZlLCByZWplY3QpIHtcclxuICAgICAgICAgICAgdmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG4gICAgICAgICAgICB4aHIub3BlbignR0VUJywgdmlldyk7XHJcbiAgICAgICAgICAgIHhoci5zZW5kKCk7XHJcbiAgICAgICAgICAgIHhoci5yZXNwb25zZVR5cGUgPSAndGV4dCc7XHJcbiAgICAgICAgICAgIHhoci5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgIGlmICh4aHIucmVhZHlTdGF0ZSA9PT0gNCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh4aHIuc3RhdHVzID09PSAyMDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSh4aHIpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlamVjdCgnRXJyb3IgcmV0cmlldmUgaHRtbCcpXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLnZpZXcgPSByZXEudGhlbihmdW5jdGlvbihkYXRhKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBkYXRhLnJlc3BvbnNlO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuZ2V0VmlldyA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnZpZXc7XHJcbiAgICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIENvbmZpZygpIHtcclxuICAgIHRoaXMuZGF0YSA9IG51bGw7XHJcbiAgICB0aGlzLnZhbCA9IG51bGw7XHJcbiAgICB0aGlzLnN0b3JhZ2VWYWx1ZSA9IG51bGw7XHJcbiAgICAvKlxyXG4gICAgdGhpcy5zZXREYXRhID0gZnVuY3Rpb24oZmlsZSkge1xyXG4gICAgICAgIHZhciByZXF1ZXN0ID0gbmV3IFByb21pc2UoZnVuY3Rpb24ocmVzb2x2ZSwgcmVqZWN0KSB7XHJcbiAgICAgICAgICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcclxuICAgICAgICAgICAgeGhyLm9wZW4oJ0dFVCcsIGZpbGUpO1xyXG4gICAgICAgICAgICB4aHIucmVzcG9uc2VUeXBlID0gJ2pzb24nLFxyXG4gICAgICAgICAgICAgICAgeGhyLm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh4aHIucmVhZHlTdGF0ZSA9PT0gNCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoeGhyLnN0YXR1cyA9PT0gMjAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnZhbCA9IHhocjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCd4aHInLCB4aHIpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHhocik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWplY3QoJ0Vycm9yIHJldHJpZXZlIGRhdGEnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgeGhyLnNlbmQobnVsbCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuZGF0YSA9IHJlcXVlc3QudGhlbihmdW5jdGlvbihkYXRhKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBkYXRhLnJlc3BvbnNlO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuZ2V0RGF0YSA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmRhdGE7XHJcbiAgICB9XHJcbiAgICAqL1xyXG5cclxuICAgIHRoaXMucmVxdWVzdERhdGEgPSBmdW5jdGlvbihmaWxlKSB7XHJcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcclxuICAgICAgICB4aHIub3ZlcnJpZGVNaW1lVHlwZSgndGV4dC9odG1sJyk7XHJcbiAgICAgICAgeGhyLm9wZW4oJ0dFVCcsIGZpbGUsIGZhbHNlKTtcclxuICAgICAgICB4aHIuc2V0UmVxdWVzdEhlYWRlcignQ29udGVudC1UeXBlJywgJ3RleHQvcGxhaW4nKTtcclxuICAgICAgICAvLyB4aHIucmVzcG9uc2VUeXBlID0gJ2pzb24nO1xyXG4gICAgICAgIHhoci5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgaWYgKHhoci5yZWFkeVN0YXRlID09PSA0KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoeGhyLnN0YXR1cyA9PT0gMjAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGRhdGEgPSB4aHIucmVzcG9uc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5zZXREYXRhKEpTT04ucGFyc2UoZGF0YSkpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnRXJyb3IgcmV0cmlldmUgZGF0YScpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHhoci5zZW5kKG51bGwpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuc2V0RGF0YSA9IGZ1bmN0aW9uKGRhdGEpIHtcclxuICAgICAgICB0aGlzLmRhdGEgPSBkYXRhXHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5nZXREYXRhID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICAvKlxyXG4gICAgICAgIGxvY2FsU3RvcmFnZSBjb25maWd1YXRpb25cclxuICAgICovXHJcblxyXG4gICAgdGhpcy5zZXQgPSBmdW5jdGlvbihuYW1lLCB2YWx1ZSkge1xyXG4gICAgICAgIHRoaXMuc3RvcmFnZVZhbHVlID0gdmFsdWU7XHJcbiAgICAgICAgdmFyIHZhbCA9IHRoaXMuc3RvcmFnZVZhbHVlO1xyXG4gICAgICAgIGlmICh0eXBlb2YgdGhpcy5zdG9yYWdlVmFsdWUgPT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgICAgIHZhbCA9IEpTT04uc3RyaW5naWZ5KHZhbHVlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKG5hbWUsIHZhbCk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5nZXQgPSBmdW5jdGlvbihuYW1lKSB7XHJcblxyXG4gICAgICAgIHJldHVybiB0eXBlb2YgdGhpcy5zdG9yYWdlVmFsdWUgPT09ICdvYmplY3QnID9cclxuICAgICAgICAgICAgSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShuYW1lKSkgOlxyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShuYW1lKTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLnJlbW92ZSA9IGZ1bmN0aW9uKG5hbWUpIHtcclxuICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShuYW1lKVxyXG4gICAgfVxyXG5cclxuICAgIC8qXHJcbiAgICAgICAgQnJvd3NlciBkZXRlY3Rpb25cclxuICAgICovXHJcblxyXG4gICAgdGhpcy5nZXRCcm93c2VyID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdmFyIG5hdkFnZW5ldCA9IHdpbmRvdy5uYXZpZ2F0b3IudXNlckFnZW50LFxyXG4gICAgICAgICAgICBicm93c2VycyA9IFsnQ2hyb21lJywgJ0ZpcmVmb3gnLCAnT3BlcmEnLCAnU2FmYXJpJywgJ0VkZ2UnLCAnTVNJRSddLFxyXG4gICAgICAgICAgICBicm93c2VyID0gJ0VkZ2UnO1xyXG5cclxuICAgICAgICBmb3IgKHZhciBpID0gYnJvd3NlcnMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHtcclxuICAgICAgICAgICAgaWYgKG5hdkFnZW5ldC5pbmRleE9mKGJyb3dzZXJzW2ldKSA+IC0xICYmIG5hdkFnZW5ldC5pbmRleE9mKCdFZGdlJykgPT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICBicm93c2VyID0gYnJvd3NlcnNbaV1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGJyb3dzZXI7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5nZXRMYW5nTG9jYXRpb24gPSBmdW5jdGlvbigpIHtcclxuICAgICAgICB2YXIgbmF2QWdlbnQgPSB3aW5kb3cubmF2aWdhdG9yLmxhbmd1YWdlcyxcclxuICAgICAgICAgICAgbGFuZyA9IG5hdkFnZW50WzBdLnNwbGl0KCctJyk7XHJcblxyXG4gICAgICAgIHJldHVybiBsYW5nWzBdO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMudGltZSA9IGZ1bmN0aW9uKGVsZW1lbnQsIGNhbGwpIHtcclxuICAgICAgICB2YXIgZGF0ZVRpbWUgPSBuZXcgRGF0ZSgpLFxyXG4gICAgICAgICAgICBkYXRlID0gZGF0ZVRpbWUuZ2V0RnVsbFllYXIoKSxcclxuICAgICAgICAgICAgaG91cnMgPSBkYXRlVGltZS5nZXRIb3VycygpLFxyXG4gICAgICAgICAgICBtaW51dGVzID0gZGF0ZVRpbWUuZ2V0TWludXRlcygpLFxyXG4gICAgICAgICAgICBzZWNvbmRzID0gZGF0ZVRpbWUuZ2V0U2Vjb25kcygpLFxyXG4gICAgICAgICAgICBmaWx0ZXJUaW1lID0gZnVuY3Rpb24odGltZSkge1xyXG4gICAgICAgICAgICAgICAgdmFyIHQgPSB0aW1lO1xyXG4gICAgICAgICAgICAgICAgaWYgKHRpbWUgPD0gOSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHQgPSBgMCR7dGltZX1gO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAvKlxyXG4gICAgICAgIHNldEludGVydmFsKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgIFxyXG4gICAgICAgICAgICBzZWNvbmRzKys7XHJcblxyXG4gICAgICAgICAgICBpZiAoc2Vjb25kcyA9PT0gNTkpIHtcclxuICAgICAgICAgICAgICAgIG1pbnV0ZXMgPSBtaW51dGVzICsgMTtcclxuICAgICAgICAgICAgICAgIHNlY29uZHMgPSAwO1xyXG4gICAgICAgICAgICAgICAgaWYgKG1pbnV0ZXMgPT09IDYwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaG91cnMgPSBob3VycyArIDE7XHJcbiAgICAgICAgICAgICAgICAgICAgbWludXRlcyA9IDA7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAoaG91cnMgPiAyNCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGhvdXJzID0gMTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gYCR7ZmlsdGVyVGltZShob3Vycyl9OiR7ZmlsdGVyVGltZShtaW51dGVzKX06JHtmaWx0ZXJUaW1lKHNlY29uZHMpfWA7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAvL2RvY3VtZW50LmdldEVsZW1lbnRCeUlkKGVsZW1lbnQpXHJcbiAgICAgICAgICAgIC8vICAgIC5pbm5lckhUTUwgPSBgJHtmaWx0ZXJUaW1lKGhvdXJzKX06JHtmaWx0ZXJUaW1lKG1pbnV0ZXMpfToke2ZpbHRlclRpbWUoc2Vjb25kcyl9YDtcclxuICAgICAgICB9LCAxMDAwKTtcclxuICAgICAgICAqL1xyXG4gICAgICAgIHJldHVybiAoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIHNlY29uZHMrKztcclxuXHJcbiAgICAgICAgICAgIGlmIChzZWNvbmRzID09PSA1OSkge1xyXG4gICAgICAgICAgICAgICAgbWludXRlcyA9IG1pbnV0ZXMgKyAxO1xyXG4gICAgICAgICAgICAgICAgc2Vjb25kcyA9IDA7XHJcbiAgICAgICAgICAgICAgICBpZiAobWludXRlcyA9PT0gNjApIHtcclxuICAgICAgICAgICAgICAgICAgICBob3VycyA9IGhvdXJzICsgMTtcclxuICAgICAgICAgICAgICAgICAgICBtaW51dGVzID0gMDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmIChob3VycyA+IDI0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaG91cnMgPSAxO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBgJHtmaWx0ZXJUaW1lKGhvdXJzKX06JHtmaWx0ZXJUaW1lKG1pbnV0ZXMpfToke2ZpbHRlclRpbWUoc2Vjb25kcyl9YDtcclxuICAgICAgICB9KSgpO1xyXG4gICAgfVxyXG59IiwiZnVuY3Rpb24gQXJ0aWNsZShkYXRhKSB7XHJcbiAgICB0aGlzLmRhdGEgPSBkYXRhO1xyXG4gICAgdGhpcy5odG1sID0gYDx1bD5cclxuICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtYWluLXRpdGxlIHRleHRDb2xvckxheW91dFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHsgdGhpcy5kYXRhLmV4cGVyaWVuY2UudGl0bGUgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHVsIGNsYXNzPVwibGlzdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICR7IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0YS5leHBlcmllbmNlLmNvbnRlbnQubWFwKGZ1bmN0aW9uKHZhbCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBgPGxpPiR7dmFsLnBvc2l0aW9ufSwgJHt2YWwuY29tcGFueX0sICR7dmFsLmNpdHkuc3RhdGV9LCAke3ZhbC5jaXR5LkNvdW50cnl9LCAke3ZhbC5zdGFydERhdGV9IC0gJHt2YWwuZW5kRGF0ZX0gPC9saT5gO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLmpvaW4oJycpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgPHVsPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1haW4tdGl0bGUgdGV4dENvbG9yTGF5b3V0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkeyB0aGlzLmRhdGEua25vd2xlZGdlLnRpdGxlIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDx1bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic3ViLXRpdGxlIHRleHRDb2xvckxheW91dFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkeyB0aGlzLmRhdGEua25vd2xlZGdlLmNvbnRlbnQuZGV2LnRpdGxlIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICR7IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kYXRhLmtub3dsZWRnZS5jb250ZW50LmRldi5jb250ZW50Lm1hcChmdW5jdGlvbih2YWwpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0aXRsZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICR7IHZhbC50aXRsZSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHVsIGNsYXNzPVwibGlzdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICR7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWwuY29udGVudC5tYXAoZnVuY3Rpb24odmFsKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGA8bGk+JHt2YWx9PC9saT5gOyAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KS5qb2luKCcnKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+YDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLmpvaW4oJycpIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInN1Yi10aXRsZSB0ZXh0Q29sb3JMYXlvdXRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHsgdGhpcy5kYXRhLmtub3dsZWRnZS5jb250ZW50Lm5ldC50aXRsZSB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzcz1cImxpc3RcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0YS5rbm93bGVkZ2UuY29udGVudC5uZXQuY29udGVudC5tYXAoZnVuY3Rpb24odmFsKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gYDxsaT4ke3ZhbH08L2xpPmA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KS5qb2luKCcnKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICA8dWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibWFpbi10aXRsZSB0ZXh0Q29sb3JMYXlvdXRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR7IHRoaXMuZGF0YS5sYW5ndWFqZXMudGl0bGUgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHVsIGNsYXNzPVwibGlzdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGEubGFuZ3VhamVzLmNvbnRlbnQubWFwKGZ1bmN0aW9uKHZhbCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBgPGxpPiR7dmFsfTwvbGk+YDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KS5qb2luKCcnKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgPC91bD5gO1xyXG5cclxuICAgIHRoaXMucmVuZGVyVmlldyA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0bWw7XHJcbiAgICB9XHJcbn0iLCJmdW5jdGlvbiBCYW5uZXIoZGF0YSkge1xyXG4gICAgdGhpcy5kYXRhID0gZGF0YTtcclxuICAgIHRoaXMuaHRtbCA9IGA8ZGl2IGNsYXNzPVwiaWNvbnMtd3JhcFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpY29ucy1sZWZ0XCIgb25jbGljaz1cIkRPTWV2ZW50cy53aWRnZXRFdmVudCgpXCI+PGkgY2xhc3M9XCJiYXJcIj48L2k+PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImljb25zLXJpZ2h0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyc3RDb2xvciBobVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgJHsgRE9NZXZlbnRzLmNvbmZpZy5nZXQoJ2NvbmZpZycpID8gXHJcbiAgICAgICAgICAgICAgICAgICAgICBgPHNwYW4+IFlvdSdyZSB1c2luZyAke0RPTWV2ZW50cy5jb25maWcuZ2V0KCdjb25maWcnKS5icm93c2VyfSBicm93c2VyPC9zcGFuPmA6ICcnIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgLyB0aW1lOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4mbmJzcDsgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicnN0Q29sb3IgaG1cIiBpZD1cInRpbWVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR7IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIURPTWV2ZW50cy50aW1lID8gYDxzbWFsbD5Mb2FkaW5nLi48L3NtYWxsPmAgOiBET01ldmVudHMudGltZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4mbmJzcCA8c3BhbiBjbGFzcz1cImhtXCI+fDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImljb24gaWNvbi1wcmludFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYXMgZmEtcHJpbnRcIiBvbmNsaWNrPSR7dGhpcy5kYXRhLmNvbmZpZy5sYW5ndWFnZSA9PT0gXCJlc1wiID8gXCJET01ldmVudHMucHJpbnRSZXN1bWUoJ2VzJylcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBcIkRPTWV2ZW50cy5wcmludFJlc3VtZSgnZW4nKVwifVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU9XCIkeyB0aGlzLmRhdGEuRE9NaW5mby5iYW5uZXJJY29ucy5wcmludC50aXRsZSB9XCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImljb24gaWNvbi1sYW5nXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cImZhcyAke3RoaXMuZGF0YS5jb25maWcubGFuZ3VhZ2UgPT09IFwiZXNcIiA/IFwiZmEtdG9nZ2xlLW9mZlwiIDogXCJmYS10b2dnbGUtb25cIn1cIiBvbmNsaWNrPVwiTGFuZy5zd2l0Y2hMYW5nKClcIiB0aXRsZT1cIiR7IHRoaXMuZGF0YS5ET01pbmZvLmJhbm5lckljb25zLmxhbmcudGl0bGUgfVwiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsYW5nXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3Bhbj4keyB0aGlzLmRhdGEuY29uZmlnLmxhbmd1YWdlLnRvVXBwZXJDYXNlKCkgfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICR7IERPTWV2ZW50cy5jb25maWcuZ2V0KCdjb25maWcnKSAmJiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIERPTWV2ZW50cy5jb25maWcuZ2V0KCdjb25maWcnKS5icm93c2VyICE9PSAnU2FmYXJpJyA/ICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGA8ZGl2IGNsYXNzPVwiaWNvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY29sb3JcImlkPVwidENvbG9yXCIgIG5hbWU9XCJjb2xvclwiIHZhbHVlPVwiIzVmOWVhMFwiIG9uY2hhbmdlPVwiRE9NZXZlbnRzLnNldFRlbXBsYXRlQ29sb3IodGhpcylcIiB0aXRsZT1cIiR7IHRoaXMuZGF0YS5ET01pbmZvLmJhbm5lckljb25zLmNvbG9yLnRpdGxlIH1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PmAgOiAnJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyc3RDb2xvclwiIG9uY2xpY2s9XCJET01ldmVudHMucmVzZXRUZW1wbGF0ZUNvbG9yKClcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgJHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdjb2xvcicpICYmIERPTWV2ZW50cy5jb25maWcuZ2V0KCdjb25maWcnKS5icm93c2VyICE9PSAnU2FmYXJpJz8gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kYXRhLmNvbmZpZy5sYW5ndWFnZSA9PT0gXCJlblwiID8gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiPHNwYW4+UmVzZXQgY29sb3I8L3NwYW4+XCI6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiPHNwYW4+UmVzdGFibGVjZXIgY29sb3I8L3NwYW4+XCIgOiAnJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PmA7XHJcblxyXG4gICAgdGhpcy5yZW5kZXJWaWV3ID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHRtbDtcclxuICAgIH1cclxufSIsImZ1bmN0aW9uIExheW91dCh2aWV3cykge1xyXG4gICAgdGhpcy5odG1sID0gYDxzZWN0aW9uIGNsYXNzPVwiY29udGFpbmVyIGNvbG9yTGF5b3V0XCIgaWQ9XCJjb250YWluZXJcIj5cclxuICAgIDxhc2lkZSBjbGFzcz1cIndpZGdldCBjb2xvckxheW91dFwiIGlkPVwid2lkZ2V0XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cIndpZGdldEJhbm5lciBjb2xvckxheW91dFwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaW1hZ2VQcm9maWxlIGNvbG9yTGF5b3V0XCI+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBvbmNsaWNrPVwiRE9NZXZlbnRzLnByZXYoKVwiIGNsYXNzPVwiY29udHJvbHNcIj48aSBjbGFzcz1cImZhcyBmYS1jaGV2cm9uLWxlZnRcIj48L2k+PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImltYWdlc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICR7IHZpZXdzWydpbWFnZSddLnJlbmRlcigpIH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgIDxzcGFuIG9uY2xpY2s9XCJET01ldmVudHMubmV4dCgpXCIgY2xhc3M9XCJjb250cm9sc1wiPjxpIGNsYXNzPVwiZmFzIGZhLWNoZXZyb24tcmlnaHRcIj48L2k+PC9zcGFuPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwid2lkZ2V0Q29udGVudFwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwid2lkZ2V0SW5mb1wiPlxyXG5cclxuICAgICAgICAgICAgICAgICR7IHZpZXdzWydwcm9maWxlJ10ucmVuZGVyKCkgfVxyXG4gICAgICAgICAgICAgICAgJHsgdmlld3NbJ3dpZGdldCddLnJlbmRlcigpIH1cclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIDwvYXNpZGU+XHJcbiAgICA8YXJpY2xlIGNsYXNzPVwiYXJ0aWNsZVwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJhcnRpY2xlQ29udGVudFwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYmFubmVyIGNvbG9yTGF5b3V0XCI+XHJcblxyXG4gICAgICAgICAgICAgICAgJHsgdmlld3NbJ2Jhbm5lciddLnJlbmRlcigpIH1cclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImV4cGVyaWVuY2VcIj5cclxuICAgICAgICAgICAgICAgICR7IHZpZXdzWydhcnRpY2xlJ10ucmVuZGVyKCkgfVxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIDwvYXJpY2xlPlxyXG48L3NlY3Rpb24+YDtcclxuXHJcbiAgICB0aGlzLnJlbmRlclZpZXcgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odG1sO1xyXG4gICAgfVxyXG59IiwiZnVuY3Rpb24gUHJvZmlsZShkYXRhKSB7XHJcbiAgICB0aGlzLmRhdGEgPSBkYXRhO1xyXG4gICAgdGhpcy5odG1sID0gYDxkaXYgY2xhc3M9XCJuYW1lXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRpdGxlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICR7IHRoaXMuZGF0YS5uYW1lIH1cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8c21hbGw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICR7IHRoaXMuZGF0YS5jYXJlZXIgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L3NtYWxsPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8dWwgY2xhc3M9XCJsaW5lXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aHIgc3R5bGU9XCJ3aWR0aDogMTAwJVwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgIDwvdWw+YDtcclxuXHJcbiAgICB0aGlzLnJlbmRlclZpZXcgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odG1sO1xyXG4gICAgfVxyXG59IiwiZnVuY3Rpb24gUHJvZmlsZUltYWdlKGRhdGEpIHtcclxuICAgIHRoaXMuZGF0YSA9IGRhdGE7XHJcbiAgICB0aGlzLmh0bWwgPSBgXHJcbiAgICAgICAgICAgICAgICAgICAgJHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kYXRhLm1hcChmdW5jdGlvbih2YWwpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiByb2xlPVwiaW1nXCIgY2xhc3M9XCJpbWFnZSAke3ZhbC5hY3RpdmUgPyBcImFjdGl2ZVwiIDogJyd9XCIgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT1cImJhY2tncm91bmQ6IHVybCgke3ZhbC5pbWd9KSBjZW50ZXIgbm8tcmVwZWF0OyBiYWNrZ3JvdW5kLXNpemU6Y292ZXJcIiBhcmlhLWxhYmU9XCJKb3NlIFJhbW9uIFJcIj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PmA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pLmpvaW4oJycpXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGA7XHJcblxyXG4gICAgdGhpcy5yZW5kZXJWaWV3ID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHRtbDtcclxuICAgIH1cclxufSIsImZ1bmN0aW9uIFdpZGdldChkYXRhKSB7XHJcbiAgICAvL2NvbnNvbGUubG9nKCdEQVRBJywgZGF0YSk7XHJcblx0dGhpcy5kYXRhID0gZGF0YTtcclxuICAgIHRoaXMuaHRtbCA9IGA8dWwgY2xhc3M9XCJsaXN0LWluZm9cIj5cclxuICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0aXRsZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHsgdGhpcy5kYXRhLmFib3V0LnRpdGxlIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDx1bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkeyB0aGlzLmRhdGEuYWJvdXQuY29udGVudCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidGl0bGVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR7IHRoaXMuZGF0YS5jb250YWN0LnRpdGxlIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDx1bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkeyB0aGlzLmRhdGEuY29udGFjdC5jb250ZW50LmJpcnRoRGF0ZS50aXRsZSB9OiAkeyB0aGlzLmRhdGEuY29udGFjdC5jb250ZW50LmJpcnRoRGF0ZS5jb250ZW50IH0sICR7IHRoaXMuZGF0YS5jb250YWN0LmNvbnRlbnQuYmlydGhQbGFjZS5zdGF0ZSB9LCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkeyB0aGlzLmRhdGEuY29udGFjdC5jb250ZW50LmJpcnRoUGxhY2UuQ291bnRyeSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICR7IHRoaXMuZGF0YS5jb250YWN0LmNvbnRlbnQuY3VycmVudEFkZHJlc3MudGl0bGUgfTogJHsgdGhpcy5kYXRhLmNvbnRhY3QuY29udGVudC5jdXJyZW50QWRkcmVzcy5jb250ZW50IH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHsgdGhpcy5kYXRhLmNvbnRhY3QuY29udGVudC5waG9uZXMudGl0bGUgfTogJHsgdGhpcy5kYXRhLmNvbnRhY3QuY29udGVudC5waG9uZXMuY29udGVudFswXSB9IC8gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHsgdGhpcy5kYXRhLmNvbnRhY3QuY29udGVudC5waG9uZXMuY29udGVudFsxXSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICR7IHRoaXMuZGF0YS5jb250YWN0LmNvbnRlbnQuZW1haWwudGl0bGUgfTogJHsgdGhpcy5kYXRhLmNvbnRhY3QuY29udGVudC5lbWFpbC5jb250ZW50IH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzcz1cInNvY2lhbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHsgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kYXRhLmNvbnRhY3QuY29udGVudC5zb2NpYWwubWFwKGZ1bmN0aW9uKHZhbCwgaSl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBgPGxpIGNsYXNzPVwiYy0ke2l9XCI+PGkgY2xhc3M9XCJmYWIgJHt2YWwuY2xhc3N9XCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3Bhbj48YSBocmVmPVwiJHt2YWwubGlua31cIiB0YXJnZXQ9XCJfQkxBTktcIiB0aXRsZT1cIiR7dmFsLnVzZXJ9XCI+JHt2YWwudXNlcn08L2E+PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+YDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KS5qb2luKCcnKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidGl0bGVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgJHsgdGhpcy5kYXRhLnNjaG9vbC50aXRsZSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAkeyBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0YS5zY2hvb2wuY29udGVudC5tYXAoZnVuY3Rpb24odmFsLGtleSl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGA8dWw+PGxpPiR7dmFsLnRpdGxlfSAvICR7dmFsLmNpdHkuc3RhdGV9LCAke3ZhbC5jaXR5LkNvdW50cnl9PC9saT48L3VsPmBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLmpvaW4oJycpIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgIDwvdWw+YDtcclxuXHJcbiAgICB0aGlzLnJlbmRlclZpZXcgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odG1sO1xyXG4gICAgfVxyXG59IiwiZnVuY3Rpb24gQXJ0aWNsZUNvbnRyb2xsZXIocGF0aCkge1xyXG4gICAgdGhpcy5wYXRoID0gcGF0aDtcclxuICAgIHRoaXMudmlldyA9IG51bGw7XHJcbn1cclxuXHJcbkFydGljbGVDb250cm9sbGVyLnByb3RvdHlwZSA9IG5ldyBDb25maWcoKTtcclxuQXJ0aWNsZUNvbnRyb2xsZXIucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uKCkge1xyXG5cclxuXHR0aGlzLnJlcXVlc3REYXRhKHRoaXMucGF0aCk7XHJcblx0cmV0dXJuIG5ldyBBcnRpY2xlKHRoaXMuZ2V0RGF0YSgpLmFydGljbGUpXHJcbiAgICAgICAgICAgIC5yZW5kZXJWaWV3KCk7XHJcblx0LypcclxuICAgIHRoaXMuc2V0RGF0YSh0aGlzLnBhdGgpO1xyXG4gICAgdGhpcy52aWV3ID0gdGhpcy5nZXREYXRhKCkudGhlbihmdW5jdGlvbihkYXRhKSB7XHJcbiAgICAgICByZXR1cm4gbmV3IFdpZGdldChkYXRhKVxyXG4gICAgICAgICAgICAucmVuZGVyVmlldygpO1xyXG4gICAgfSk7XHJcbiAgICAqL1xyXG59IiwiZnVuY3Rpb24gQmFubmVyQ29udHJvbGxlcihwYXRoKSB7XHJcbiAgICB0aGlzLnBhdGggPSBwYXRoO1xyXG59XHJcblxyXG5CYW5uZXJDb250cm9sbGVyLnByb3RvdHlwZSA9IG5ldyBDb25maWcoKTtcclxuQmFubmVyQ29udHJvbGxlci5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24oKSB7XHJcbiAgICB0aGlzLnJlcXVlc3REYXRhKHRoaXMucGF0aCk7XHJcbiAgICByZXR1cm4gbmV3IEJhbm5lcih0aGlzLmdldERhdGEoKSlcclxuICAgICAgICAucmVuZGVyVmlldygpO1xyXG59IiwiZnVuY3Rpb24gTGF5b3V0Q29udHJvbGxlcih2aWV3cykge1xyXG4gICAgLy8gdGhpcy5wYXRoID0gcGF0aDtcclxuICAgIHRoaXMudmlld3MgPSB2aWV3cztcclxufVxyXG5cclxuLy8gTGF5b3V0Q29udHJvbGxlci5wcm90b3R5cGUgPSBuZXcgQ29uZmlnKCk7XHJcbkxheW91dENvbnRyb2xsZXIucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uKCkge1xyXG4gICAgLy8gdGhpcy5yZXF1ZXN0RGF0YSh0aGlzLnBhdGgpO1xyXG4gICAgcmV0dXJuIG5ldyBMYXlvdXQodGhpcy52aWV3cylcclxuICAgICAgICAucmVuZGVyVmlldygpO1xyXG59IiwiZnVuY3Rpb24gUHJvZmlsZUNvbnRyb2xsZXIocGF0aCkge1xyXG4gICAgdGhpcy5wYXRoID0gcGF0aDtcclxufVxyXG5cclxuUHJvZmlsZUNvbnRyb2xsZXIucHJvdG90eXBlID0gbmV3IENvbmZpZygpO1xyXG5Qcm9maWxlQ29udHJvbGxlci5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24oKSB7XHJcbiAgICB0aGlzLnJlcXVlc3REYXRhKHRoaXMucGF0aCk7XHJcbiAgICByZXR1cm4gbmV3IFByb2ZpbGUodGhpcy5nZXREYXRhKCkud2lkZ2V0LnByb2ZpbGUpXHJcbiAgICAgICAgLnJlbmRlclZpZXcoKTtcclxufSIsImZ1bmN0aW9uIFByb2ZpbGVJbWFnZUNvbnRyb2xsZXIocGF0aCkge1xyXG4gICAgdGhpcy5wYXRoID0gcGF0aDtcclxufVxyXG5cclxuUHJvZmlsZUltYWdlQ29udHJvbGxlci5wcm90b3R5cGUgPSBuZXcgQ29uZmlnKCk7XHJcblByb2ZpbGVJbWFnZUNvbnRyb2xsZXIucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uKCkge1xyXG4gICAgdGhpcy5yZXF1ZXN0RGF0YSh0aGlzLnBhdGgpO1xyXG4gICAgcmV0dXJuIG5ldyBQcm9maWxlSW1hZ2UodGhpcy5nZXREYXRhKCkud2lkZ2V0LnByb2ZpbGUuaW1hZ2VzKVxyXG4gICAgICAgIC5yZW5kZXJWaWV3KCk7XHJcbn0iLCJmdW5jdGlvbiBXaWRnZXRDb250cm9sbGVyKHBhdGgpIHtcclxuICAgIHRoaXMucGF0aCA9IHBhdGg7XHJcbiAgICB0aGlzLnZpZXcgPSBudWxsO1xyXG59XHJcblxyXG5XaWRnZXRDb250cm9sbGVyLnByb3RvdHlwZSA9IG5ldyBDb25maWcoKTtcclxuV2lkZ2V0Q29udHJvbGxlci5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24oKSB7XHJcblxyXG5cdHRoaXMucmVxdWVzdERhdGEodGhpcy5wYXRoKTtcclxuXHRyZXR1cm4gbmV3IFdpZGdldCh0aGlzLmdldERhdGEoKS53aWRnZXQpXHJcbiAgICAgICAgICAgIC5yZW5kZXJWaWV3KCk7XHJcblx0LypcclxuICAgIHRoaXMuc2V0RGF0YSh0aGlzLnBhdGgpO1xyXG4gICAgdGhpcy52aWV3ID0gdGhpcy5nZXREYXRhKCkudGhlbihmdW5jdGlvbihkYXRhKSB7XHJcbiAgICAgICByZXR1cm4gbmV3IFdpZGdldChkYXRhKVxyXG4gICAgICAgICAgICAucmVuZGVyVmlldygpO1xyXG4gICAgfSk7XHJcbiAgICAqL1xyXG59IiwidmFyIERPTWV2ZW50cyA9IHtcclxuICAgIHZpZXdWYWx1ZTogbnVsbCxcclxuICAgIGJyb3dzZXI6IG51bGwsXHJcbiAgICBjb25maWc6IG5ldyBDb25maWcoKSxcclxuICAgIGxhbmc6IG5ldyBMYW5nKCksXHJcblxyXG4gICAgaW5pdERPTXZhbHVlczogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdjb2xvcicpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0VGVtcGxhdGVDb2xvcih1bmRlZmluZWQpO1xyXG4gICAgICAgICAgICAvLyBkb2N1bWVudC5ib2R5LnN0eWxlLnNldFByb3BlcnR5KCctLWNvbG9yJywgSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnY29sb3InKSkuY29sb3IpO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyB0aGlzLmNvbmZpZy50aW1lKCd0aW1lJyk7XHJcbiAgICAgICAgc2V0SW50ZXJ2YWwoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCd0aW1lJylcclxuICAgICAgICAgICAgICAgIC5pbm5lckhUTUwgPSB0aGlzLmNvbmZpZy50aW1lKCd0aW1lJyk7XHJcbiAgICAgICAgfS5iaW5kKHRoaXMpLCAxMDAwKTtcclxuXHJcbiAgICAgICAgdGhpcy5icm93c2VyID0gdGhpcy5jb25maWcuZ2V0QnJvd3NlcigpO1xyXG4gICAgICAgIHRoaXMud1Jlc2l6ZSgpO1xyXG4gICAgfSxcclxuICAgIGNhcnJvdXNlbENvbnRyb2xsczogZnVuY3Rpb24odmFsdWUpIHtcclxuICAgICAgICB2YXIgZWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5pbWFnZXMnKTtcclxuICAgICAgICB2YXIgYUVsZW1lbnQgPSBudWxsO1xyXG5cclxuICAgICAgICBmb3IgKHZhciBpID0gZWxlbWVudC5jaGlsZHJlbi5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xyXG5cclxuICAgICAgICAgICAgaWYgKGVsZW1lbnQuY2hpbGRyZW5baV0uY2xhc3NMaXN0LmNvbnRhaW5zKCdhY3RpdmUnKSkge1xyXG4gICAgICAgICAgICAgICAgYUVsZW1lbnQgPSBlbGVtZW50LmNoaWxkcmVuW2ldW3ZhbHVlID09PSAnbmV4dCcgPyAnbmV4dEVsZW1lbnRTaWJsaW5nJyA6ICdwcmV2aW91c0VsZW1lbnRTaWJsaW5nJ107XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGVsZW1lbnQuY2hpbGRyZW5baV0uY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIWFFbGVtZW50KSB7XHJcbiAgICAgICAgICAgIGVsZW1lbnQuY2hpbGRyZW5bdmFsdWUgPT09ICduZXh0JyA/IDAgOiBlbGVtZW50LmNoaWxkcmVuLmxlbmd0aCAtIDFdLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGFFbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgc2V0VGVtcGxhdGVDb2xvcjogZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIHZhciBjb2xvclZhbHVlID0gZSA/IGUudmFsdWUgOiBudWxsIHx8IHRoaXMuY29uZmlnLmdldCgnY29sb3InKS5jb2xvcjtcclxuXHJcbiAgICAgICAgdmFyIHNldFZhbHVlID0gZnVuY3Rpb24oYXJyLCBydWxlKSB7XHJcbiAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gYXJyLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXJyW2ldLnN0eWxlW3J1bGVdID0gY29sb3JWYWx1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgYmFja2dyb3VuZCA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2NvbG9yTGF5b3V0JyksXHJcbiAgICAgICAgICAgIGNvbG9yID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgndGV4dENvbG9yTGF5b3V0Jyk7XHJcblxyXG5cclxuXHJcbiAgICAgICAgdGhpcy5jb25maWcuc2V0KCdjb2xvcicsIHsgY29sb3I6IGNvbG9yVmFsdWUgfSk7XHJcblxyXG4gICAgICAgIGlmIChlICYmIGUudmFsdWUpIHtcclxuICAgICAgICAgICAgcmVuZGVyKHRoaXMuY29uZmlnLmdldCgnY29uZmlnJykubGFuZyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBzZXRWYWx1ZShiYWNrZ3JvdW5kLCAnYmFja2dyb3VuZENvbG9yJyk7XHJcbiAgICAgICAgLy8gc2V0VmFsdWUoY29sb3IsICdjb2xvcicpO1xyXG5cclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICBpZiAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3RDb2xvcicpKSB7XHJcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgndENvbG9yJykudmFsdWUgPSBjb2xvclZhbHVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGRvY3VtZW50LmJvZHkuc3R5bGUuc2V0UHJvcGVydHkoJy0tY29sb3InLCBjb2xvclZhbHVlKTtcclxuICAgICAgICB9LCAxMCk7XHJcblxyXG5cclxuICAgIH0sXHJcbiAgICBnZXRWYWx1ZUZyb21WaWV3OiBmdW5jdGlvbih2YWx1ZSkge1xyXG4gICAgICAgIHRoaXMudmlld1ZhbHVlID0gdmFsdWU7XHJcbiAgICB9LFxyXG4gICAgcHJpbnRSZXN1bWU6IGZ1bmN0aW9uKGxhbmcpIHtcclxuICAgICAgICB2YXIgbXNnID0gbGFuZyA9PT0gJ2VzJyA/ICdQYXJhIG1lam9yIGNhbGlkYWQgZGUgaW1wcmVzacOzbiBzZSByZWNvbWllbmRhIHVzYXIgR29vZ2xlIGNocm9tZScgOlxyXG4gICAgICAgICAgICAnRm9yIGJldHRlciBxdWFsaXR5IHByaW50aW5nLCBwbGVhc2UgdXNlIEdvb2dsZSBjaHJvbWUnO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5icm93c2VyID09PSAnRWRnZScgfHwgdGhpcy5icm93c2VyID09PSAnRmlyZWZveCcpIHtcclxuICAgICAgICAgICAgaWYgKGNvbmZpcm0obXNnKSkge1xyXG4gICAgICAgICAgICAgICAgYWxlcnQoJ1NvcnJ5IHdlIGNhbm5vdCBvcGVuIHBhZ2UgaW4gY2hyb21lIDooIFxcbiBwbGVhc2Ugb3BlbiBhcHAgbWFudWFsbHkgaW4gQ2hyb21lIGJyb3dzZXInKTtcclxuICAgICAgICAgICAgICAgIHdpbmRvdy5vcGVuKCdodHRwczovL3d3dy5nb29nbGUuY29tL2Nocm9tZScpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgd2luZG93LnByaW50KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB3aW5kb3cucHJpbnQoKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgcmVzZXRUZW1wbGF0ZUNvbG9yOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLnNldFRlbXBsYXRlQ29sb3IoeyB2YWx1ZTogJyM1ZjllYTAnIH0pO1xyXG4gICAgICAgIHRoaXMuY29uZmlnLnJlbW92ZSgnY29sb3InKVxyXG4gICAgICAgIHJlbmRlcih0aGlzLmNvbmZpZy5nZXQoJ2NvbmZpZycpLmxhbmcpO1xyXG4gICAgfSxcclxuICAgIG5leHQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuY2Fycm91c2VsQ29udHJvbGxzKCduZXh0Jyk7XHJcbiAgICB9LFxyXG4gICAgcHJldjogZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgIHRoaXMuY2Fycm91c2VsQ29udHJvbGxzKCdwcmV2Jyk7XHJcbiAgICB9LFxyXG5cclxuICAgIHdpZGdldEV2ZW50OiBmdW5jdGlvbigpIHtcclxuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnY29udGFpbmVyJylcclxuICAgICAgICAgICAgLmNsYXNzTGlzdC50b2dnbGUoJ21vYmlsZScpO1xyXG4gICAgfSxcclxuXHJcbiAgICB3UmVzaXplOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB2YXIgdyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjb250YWluZXInKTtcclxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA+IDc2MCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHcuY2xhc3NMaXN0LmNvbnRhaW5zKCdtb2JpbGUnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHcuY2xhc3NMaXN0LnJlbW92ZSgnbW9iaWxlJylcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0uYmluZCh0aGlzKSk7XHJcbiAgICB9XHJcblxyXG59IiwiZnVuY3Rpb24gRXZlbnRzKCkge1xyXG4gICAgdGhpcy5vYnNlcnZlcnMgPSBbXTtcclxufVxyXG5FdmVudHMucHJvdG90eXBlLnN1c2NyaWJlID0gZnVuY3Rpb24oY2FsbCkge1xyXG4gICAgdGhpcy5vYnNlcnZlcnMucHVzaChjYWxsKTtcclxufVxyXG5FdmVudHMucHJvdG90eXBlLnVuc3VzY3JpYmUgPSBmdW5jdGlvbihjYWxsKSB7XHJcbiAgICBpZiAodGhpcy5vYnNlcnZlcnMubGVuZ3RoKSB7XHJcbiAgICAgICAgdmFyIGluZGV4ID0gdGhpcy5vYnNlcnZlcnMuaW5kZXhPZihjYWxsKTtcclxuICAgICAgICB0aGlzLm9ic2VydmVycy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgfVxyXG59XHJcbkV2ZW50cy5wcm90b3R5cGUubm90aWZ5T2JzZXJ2ZXIgPSBmdW5jdGlvbih2YWx1ZSkge1xyXG5cclxuICAgIGlmICh0aGlzLm9ic2VydmVycy5sZW5ndGgpIHtcclxuICAgICAgICB0aGlzLm9ic2VydmVyc1swXSh2YWx1ZSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIExhbmcoKSB7XHJcblx0dGhpcy5ldmVudHMgPSBuZXcgRXZlbnRzKCk7XHJcbiAgICB0aGlzLmNvbmZpZyA9IG5ldyBDb25maWcoKTtcclxuXHR0aGlzLmlzRVMgPSB0aGlzLmNvbmZpZy5nZXQoJ2NvbmZpZycpICYmIHRoaXMuY29uZmlnLmdldCgnY29uZmlnJykubGFuZyA9PT0gJ2VuJyA/IGZhbHNlIDogdHJ1ZSxcclxuICAgIHRoaXMubGFuZyA9ICdlcyc7XHJcbn1cclxuXHJcblxyXG5MYW5nLnByb3RvdHlwZS5zd2l0Y2hMYW5nID0gZnVuY3Rpb24oKSB7XHJcblx0dGhpcy5pc0VTID0gIXRoaXMuaXNFUztcclxuICAgIHRoaXMubGFuZyA9IHRoaXMuaXNFUyA/ICdlcycgOiAnZW4nO1xyXG5cdHRoaXMuZXZlbnRzLm5vdGlmeU9ic2VydmVyKHRoaXMubGFuZyk7XHJcblxyXG4gICAgcmV0dXJuIHRoaXMubGFuZztcclxufVxyXG5cclxuXHJcbkxhbmcucHJvdG90eXBlLmdldExhbmcgPSBmdW5jdGlvbigpIHtcclxuICAgIHJldHVybiB0aGlzLmlzRVM7XHJcbn1cclxuXHJcbi8vdmFyIGV2ID0gbmV3IEV2ZW50cygpO1xyXG5cclxuXHJcbi8qXHJcbmZ1bmN0aW9uIG5vdGlmeShmbikge1xyXG5cdGV2LnN1c2NyaWJlKGZuKTtcclxuXHRldi5ub3RpZnlPYnNlcnZlcigpO1xyXG59XHJcbiovXHJcblxyXG4vKlxyXG52YXIgZXZlbnRzID0ge1xyXG5cdGlzRVM6IHRydWUsXHJcbiAgICBzZXRMYW5nOiBmdW5jdGlvbigpIHtcclxuICAgIFx0dGhpcy5pc0VTID0gIXRoaXMuaXNFUztcclxuICAgIH0sXHJcbiAgICBnZXRMYW5nOiBmdW5jdGlvbigpIHtcclxuICAgIFx0cmV0dXJuIHRoaXMuaXNFU1xyXG4gICAgfVxyXG59XHJcblxyXG4qLyIsImZ1bmN0aW9uIEFwcChsYW5nKSB7XHJcbiAgICB0aGlzLmxhbmcgPSBsYW5nO1xyXG4gICAgdGhpcy5wYXRoID0ge1xyXG4gICAgICAgIGVzOiAnLi9kYXRhL2VzLmpzb24nLFxyXG4gICAgICAgIGVuOiAnLi9kYXRhL2VuLmpzb24nLFxyXG4gICAgfVxyXG4gICAgLy8gdGhpcy5wYXRoID0gdmFsdWUgPT09ICdlcycgPyAnLi9kYXRhL2VzLmpzb24nIDogJy4vZGF0YS9lbi5qc29uJztcclxufVxyXG5cclxuQXBwLnByb3RvdHlwZSA9IG5ldyBDb25maWcoKTtcclxuXHJcbkFwcC5wcm90b3R5cGUudmlld3MgPSBmdW5jdGlvbigpIHtcclxuICAgIHZhciBwYXRoID0gdGhpcy5sYW5nID8gdGhpcy5wYXRoW3RoaXMubGFuZ10gOiB0aGlzLnBhdGhbJ2VzJ107XHJcblxyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBhcnRpY2xlOiBuZXcgQXJ0aWNsZUNvbnRyb2xsZXIocGF0aCksXHJcbiAgICAgICAgd2lkZ2V0OiBuZXcgV2lkZ2V0Q29udHJvbGxlcihwYXRoKSxcclxuICAgICAgICBwcm9maWxlOiBuZXcgUHJvZmlsZUNvbnRyb2xsZXIocGF0aCksXHJcbiAgICAgICAgYmFubmVyOiBuZXcgQmFubmVyQ29udHJvbGxlcihwYXRoKSxcclxuICAgICAgICBpbWFnZTogbmV3IFByb2ZpbGVJbWFnZUNvbnRyb2xsZXIocGF0aCksXHJcbiAgICB9XHJcbn1cclxuXHJcbkFwcC5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24odmlldykge1xyXG4gICAgdGhpcy5zZXQoJ2NvbmZpZycsIHtcclxuICAgICAgICBsYW5nOiB0aGlzLmxhbmcsXHJcbiAgICAgICAgYnJvd3NlcjogdGhpcy5nZXRCcm93c2VyKClcclxuICAgIH0pO1xyXG5cclxuICAgIHJldHVybiBuZXcgTGF5b3V0Q29udHJvbGxlcih0aGlzLnZpZXdzKCkpXHJcbiAgICAgICAgLnJlbmRlcigpO1xyXG59Il19
