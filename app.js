function App(lang) {
    this.lang = lang;
    this.path = {
        es: './data/es.json',
        en: './data/en.json',
    }
    // this.path = value === 'es' ? './data/es.json' : './data/en.json';
}

App.prototype = new Config();

App.prototype.views = function() {
    var path = this.lang ? this.path[this.lang] : this.path['es'];

    return {
        article: new ArticleController(path),
        widget: new WidgetController(path),
        profile: new ProfileController(path),
        banner: new BannerController(path),
        image: new ProfileImageController(path),
    }
}

App.prototype.render = function(view) {
    this.set('config', {
        lang: this.lang,
        browser: this.getBrowser()
    });

    return new LayoutController(this.views())
        .render();
}