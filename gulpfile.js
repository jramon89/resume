var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    minifyCSS = require('gulp-csso'),
    cleanCSS = require('gulp-clean-css'),
    sourcemaps = require('gulp-sourcemaps'),
    inject = require('gulp-inject'),
    minify = require('gulp-minify'),
    clean = require('gulp-clean');


gulp.task('js', function() {
    return gulp.src([
            'public/config/*.js',
            'views/*.js',
            'controllers/*.js',
            'public/*js',
            './app.js'
        ])
        .pipe(sourcemaps.init())
        .pipe(concat('main.js'))
        .pipe(minify())
        .pipe(sourcemaps.write())
        .pipe(clean())
        .pipe(gulp.dest('build'));
})

gulp.task('css', function() {
    return gulp.src('public/style.css')
        .pipe(concat('main.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('build'));
})

gulp.task('css-print', function() {
    return gulp.src('public/style-print.css')
        .pipe(concat('main-print.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('build'));
})

gulp.task('images', function() {
    return gulp.src('public/images/*')
        .pipe(gulp.dest('build/images'));
})

gulp.task('data',['images'], function() {
    return gulp.src('data/*')
        .pipe(gulp.dest('build/data'));
})

gulp.task('html', function() {
    return gulp.src('public/*.html')
        .pipe(gulp.dest('build'));
})

/**************************DEV Config***********************************/



gulp.task('build', ['js', 'css', 'css-print', 'data', 'html']);