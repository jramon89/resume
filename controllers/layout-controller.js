function LayoutController(views) {
    // this.path = path;
    this.views = views;
}

// LayoutController.prototype = new Config();
LayoutController.prototype.render = function() {
    // this.requestData(this.path);
    return new Layout(this.views)
        .renderView();
}