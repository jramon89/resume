function ProfileImageController(path) {
    this.path = path;
}

ProfileImageController.prototype = new Config();
ProfileImageController.prototype.render = function() {
    this.requestData(this.path);
    return new ProfileImage(this.getData().widget.profile.images)
        .renderView();
}