function BannerController(path) {
    this.path = path;
}

BannerController.prototype = new Config();
BannerController.prototype.render = function() {
    this.requestData(this.path);
    return new Banner(this.getData())
        .renderView();
}