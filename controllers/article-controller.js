function ArticleController(path) {
    this.path = path;
    this.view = null;
}

ArticleController.prototype = new Config();
ArticleController.prototype.render = function() {

	this.requestData(this.path);
	return new Article(this.getData().article)
            .renderView();
	/*
    this.setData(this.path);
    this.view = this.getData().then(function(data) {
       return new Widget(data)
            .renderView();
    });
    */
}