function WidgetController(path) {
    this.path = path;
    this.view = null;
}

WidgetController.prototype = new Config();
WidgetController.prototype.render = function() {

	this.requestData(this.path);
	return new Widget(this.getData().widget)
            .renderView();
	/*
    this.setData(this.path);
    this.view = this.getData().then(function(data) {
       return new Widget(data)
            .renderView();
    });
    */
}