function ProfileController(path) {
    this.path = path;
}

ProfileController.prototype = new Config();
ProfileController.prototype.render = function() {
    this.requestData(this.path);
    return new Profile(this.getData().widget.profile)
        .renderView();
}