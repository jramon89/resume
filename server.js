const express = require('express');
const path = require('path');
const app = express();
const port = process.env.PORT || 8000;


//app.use(express.static(path.join(__dirname,'/public')))
app.use(express.static(path.join(__dirname,'/build')))
// app.use(express.static(path.join(__dirname,'/')))
// app.use(express.static(path.join(__dirname,'/views')))

app.get('/', function(req, res){
	res.sendFile(path.join(__dirname, 'build/index.html'));
});
app.listen(port, function() {
	console.log('Server listening on port: '+port);
})


/*var http = require('http');
var fs = require('fs');

http.createServer(function(req, res) {
    fs.readFile('./index.html', function(err, data) {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write(data);
        res.end();
    });
}).listen(8000)*/