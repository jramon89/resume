function Profile(data) {
    this.data = data;
    this.html = `<div class="name">
                    <div class="title">
                        ${ this.data.name }
                    </div>
                    <small>
                        ${ this.data.career }
                        </small>
                </div>
                <ul class="line">
                    <li>
                        <hr style="width: 100%" />
                    </li>
                </ul>`;

    this.renderView = function() {
        return this.html;
    }
}