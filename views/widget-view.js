function Widget(data) {
    //console.log('DATA', data);
	this.data = data;
    this.html = `<ul class="list-info">
                    <li>
                        <div class="title">
                            ${ this.data.about.title }
                        </div>
                        <ul>
                            <li>
                                ${ this.data.about.content }
                            </li>
                        </ul>
                        <div class="title">
                            ${ this.data.contact.title }
                        </div>
                        <ul>
                            <li>
                                ${ this.data.contact.content.birthDate.title }: ${ this.data.contact.content.birthDate.content }, ${ this.data.contact.content.birthPlace.state }, 
                                ${ this.data.contact.content.birthPlace.Country }
                            </li>
                            <li>
                                ${ this.data.contact.content.currentAddress.title }: ${ this.data.contact.content.currentAddress.content }
                            </li>
                            <li>
                                ${ this.data.contact.content.phones.title }: ${ this.data.contact.content.phones.content[0] } / 
                                ${ this.data.contact.content.phones.content[1] }
                            </li>
                            <li>
                                ${ this.data.contact.content.email.title }: ${ this.data.contact.content.email.content }
                            </li>
                        </ul>
                        <ul class="social">
                            ${ 
                                this.data.contact.content.social.map(function(val, i){
                                    return `<li class="c-${i}"><i class="fab ${val.class}"></i>
                                                <span><a href="${val.link}" target="_BLANK" title="${val.user}">${val.user}</a></span>
                                            </li>`;
                                }).join('')
                            }
                        </ul>
                    </li>
                    <li>
                        <div class="title">
                           ${ this.data.school.title }
                        </div>
                        ${ 
                            this.data.school.content.map(function(val,key){
                                return `<ul><li>${val.title} / ${val.city.state}, ${val.city.Country}</li></ul>`
                            }).join('') 
                        }
                    </li>
                </ul>`;

    this.renderView = function() {
        return this.html;
    }
}