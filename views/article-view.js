function Article(data) {
    this.data = data;
    this.html = `<ul>
                    <li>
                        <div class="main-title textColorLayout">
                            ${ this.data.experience.title }
                        </div>
                        <ul class="list">
                             ${ 
                                this.data.experience.content.map(function(val){
                                    return `<li>${val.position}, ${val.company}, ${val.city.state}, ${val.city.Country}, ${val.startDate} - ${val.endDate} </li>`;
                                }).join('')
                             }
                        </ul>
                    </li>
                </ul>
                <ul>
                    <li>
                        <div class="main-title textColorLayout">
                            ${ this.data.knowledge.title }
                        </div>
                        <ul>
                            <li>
                                <div class="sub-title textColorLayout">
                                    ${ this.data.knowledge.content.dev.title }
                                </div>
                                <ul>
                                    ${ 
                                        this.data.knowledge.content.dev.content.map(function(val){
                                            return `<li>
                                                        <div class="title">
                                                           ${ val.title }
                                                        </div>
                                                        <ul class="list">
                                                           ${
                                                                val.content.map(function(val){
                                                                     return `<li>${val}</li>`;  
                                                                }).join('')
                                                            }
                                                        </ul>
                                                    </li>`;
                                        }).join('') 
                                    }
                                </ul>
                            </li>
                            <li>
                                <div class="sub-title textColorLayout">
                                    ${ this.data.knowledge.content.net.title }

                                </div>
                                <ul class="list">
                                    ${
                                        this.data.knowledge.content.net.content.map(function(val){
                                            return `<li>${val}</li>`;
                                        }).join('')
                                    }
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul>
                    <li>
                        <div class="main-title textColorLayout">
                            ${ this.data.languajes.title }
                        </div>
                        <ul class="list">
                            ${
                                this.data.languajes.content.map(function(val){
                                    return `<li>${val}</li>`;
                                }).join('')
                             }
                        </ul>
                    </li>
                </ul>`;

    this.renderView = function() {
        return this.html;
    }
}