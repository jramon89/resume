function Banner(data) {
    this.data = data;
    this.html = ` <div class="carrousel">
                    <div class="carrousel-content">
                        <span onclick="DOMevents.prev()">Prev</span>
                        <div class="carrousel-items">
                            <div class="carrousel-item active">
                                <h1>A</h1>
                            </div>
                            <div class="carrousel-item">
                                <h1>B</h1>
                            </div>
                            <div class="carrousel-item">
                                <h1>C</h1>
                            </div>
                        </div>
                        <span onclick="DOMevents.next()">Next</span>
                    </div>
                </div>`;

    this.renderView = function() {
        return this.html;
    }
}