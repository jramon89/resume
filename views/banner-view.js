function Banner(data) {
    this.data = data;
    this.html = `<div class="icons-wrap">
                    <div class="icons-left" onclick="DOMevents.widgetEvent()"><i class="bar"></i></div>
                    <div class="icons-right">
                        <div class="rstColor hm">
                      ${ DOMevents.config.get('config') ? 
                      `<span> You're using ${DOMevents.config.get('config').browser} browser</span>`: '' }
                        / time:
                        </div>&nbsp;  
                        <div class="rstColor hm" id="time">
                            ${ 
                               !DOMevents.time ? `<small>Loading..</small>` : DOMevents.time
                            }
                        </div>&nbsp <span class="hm">|</span>
                        <div class="icon icon-print">
                            <i class="fas fa-print" onclick=${this.data.config.language === "es" ? "DOMevents.printResume('es')"
                            : "DOMevents.printResume('en')"}
                            title="${ this.data.DOMinfo.bannerIcons.print.title }"></i>
                        </div>
                        <div class="icon icon-lang">
                            <i class="fas ${this.data.config.language === "es" ? "fa-toggle-off" : "fa-toggle-on"}" onclick="Lang.switchLang()" title="${ this.data.DOMinfo.bannerIcons.lang.title }"></i>
                        </div>
                        <div class="lang">
                            <span>${ this.data.config.language.toUpperCase() }</span>
                        </div>
                        ${ DOMevents.config.get('config') && 
                            DOMevents.config.get('config').browser !== 'Safari' ?  
                            `<div class="icon">
                                <input type="color"id="tColor"  name="color" value="#5f9ea0" onchange="DOMevents.setTemplateColor(this)" title="${ this.data.DOMinfo.bannerIcons.color.title }">
                            </div>` : ''
                        }
                        <div class="rstColor" onclick="DOMevents.resetTemplateColor()">
                        ${
                            localStorage.getItem('color') && DOMevents.config.get('config').browser !== 'Safari'? 
                                this.data.config.language === "en" ? 
                                    "<span>Reset color</span>":
                                    "<span>Restablecer color</span>" : ''
                        }
                        </div>
                        </div>
                </div>`;

    this.renderView = function() {
        return this.html;
    }
}