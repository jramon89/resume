function Layout(views) {
    this.html = `<section class="container colorLayout" id="container">
    <aside class="widget colorLayout" id="widget">
        <div class="widgetBanner colorLayout">
            <div class="imageProfile colorLayout">
                <span onclick="DOMevents.prev()" class="controls"><i class="fas fa-chevron-left"></i></span>
                <div class="images">
                    ${ views['image'].render() }
                </div>

                <span onclick="DOMevents.next()" class="controls"><i class="fas fa-chevron-right"></i></span>
            </div>
        </div>
        <div class="widgetContent">
            <div class="widgetInfo">

                ${ views['profile'].render() }
                ${ views['widget'].render() }
                
            </div>
        </div>
    </aside>
    <aricle class="article">
        <div class="articleContent">
            <div class="banner colorLayout">

                ${ views['banner'].render() }
                
            </div>
            <div class="experience">
                ${ views['article'].render() }
            </div>
        </div>
    </aricle>
</section>`;

    this.renderView = function() {
        return this.html;
    }
}