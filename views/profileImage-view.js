function ProfileImage(data) {
    this.data = data;
    this.html = `
                    ${
                        this.data.map(function(val){
                            return `
                                    <div role="img" class="image ${val.active ? "active" : ''}" 
                                        style="background: url(${val.img}) center no-repeat; background-size:cover" aria-labe="Jose Ramon R">

                                    </div>`;
                        }).join('')
                    }
                    `;

    this.renderView = function() {
        return this.html;
    }
}